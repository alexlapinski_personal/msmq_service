﻿using System;
using System.Messaging;

namespace msmq_listener
{
    public class MessageListener
    {
        private MessageQueue _queue;

        public MessageListener()
        {
            var path = ".\\Private$\\SampleQueue";
            _queue = MessageQueue.Exists(path) ? new MessageQueue(path) : MessageQueue.Create(path);
            _queue.Formatter = new XmlMessageFormatter(new Type[] { typeof(String) });
            _queue.ReceiveCompleted += _queue_ReceiveCompleted;
        }

        private void _queue_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            var message =_queue.EndReceive(e.AsyncResult);
            Console.WriteLine(message.Body);
            _queue.BeginReceive();
        }

        public void Start()
        {
            _queue.BeginReceive();
        }

        public void Stop()
        {
            _queue.Dispose();
        }
    }
}
