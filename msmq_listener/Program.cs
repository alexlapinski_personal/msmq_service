﻿using Topshelf;

namespace msmq_listener
{
    public class Program
    {
        public static void Main(string[] args)
        {
            HostFactory.Run(x =>                                 
            {
                x.Service<MessageListener>(s =>
                {
                    s.ConstructUsing(name => new MessageListener());     
                    s.WhenStarted(tc => tc.Start());             
                    s.WhenStopped(tc => tc.Stop());               
                });
                x.RunAsLocalSystem();

                x.SetDescription("Message Listener");
                x.SetDisplayName("MessageListener");
                x.SetServiceName("message_listener");
            });
        }
    }
}
