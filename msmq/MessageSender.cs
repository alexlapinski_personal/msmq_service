﻿namespace msmq
{
    using System.Messaging;
    using System.Timers;

    public class MessageSender
    {
        private MessageQueue _queue;
        private Timer _timer;
        private long _iteration;

        public MessageSender()
        {
            var path = ".\\Private$\\SampleQueue";
            _queue = MessageQueue.Exists(path) ? new MessageQueue(path) : MessageQueue.Create(path);
            _timer = new Timer(1000) { AutoReset = true };

            _timer.Elapsed += (sender, eventArgs) =>
            {
                _queue.Send(new Message
                {
                    Body = "Iteration #" + _iteration,
                    Label = "Iteration #" + _iteration
                });
                _iteration++;
            };
        }

        public void Start()
        {
            _iteration = 0;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
            _queue.Dispose();
        }
    }
}
