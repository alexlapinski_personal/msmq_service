﻿using Topshelf;

namespace msmq
{
    public class Program
    {
        public static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<MessageSender>(s =>
                {
                    s.ConstructUsing(name => new MessageSender());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();
                x.StartAutomatically();

                x.SetDescription("Message Sender");
                x.SetDisplayName("MessageSender");
                x.SetServiceName("message_sender");
            });
        }
    }
}
